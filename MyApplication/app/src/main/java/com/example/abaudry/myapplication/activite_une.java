package com.example.abaudry.myapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ImageView;
import android.widget.TextView;

public class activite_une extends AppCompatActivity {

    private Integer minFemme = 15; //maigreur si en dessous
    private Integer maxFemme = 30; //graisse si au-dessus
    private Integer minHomme = 10; //maigreur si en dessous
    private Integer maxHomme = 25; // graisse si au-dessus
    public int sexe = 1; //0 pour femme, 1 pour homme

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activite_une);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        this.ecouteRadio();
        this.ecouteCalculer();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activite_une, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void ecouteRadio() {
        ((RadioGroup) findViewById(R.id.gbSexe)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (((RadioButton) findViewById(R.id.rbHomme)).isChecked()) {
                    sexe = 1;
                } else {
                    sexe = 0;
                }
            }
        });
    }

    private void ecouteCalculer() {
        ((Button) findViewById(R.id.btnCalculer)).setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                String txtPoids = ((EditText) findViewById(R.id.etPoids)).getText().toString();
                String txtTaille = ((EditText) findViewById(R.id.etTaille)).getText().toString();
                String txtAge = ((EditText) findViewById(R.id.etAge)).getText().toString();
                TextView lblIMG = (TextView) findViewById(R.id.tvResult);
                ImageView imgSmiley = (ImageView) findViewById(R.id.imageView);

                if (!(txtPoids.equals("")) && !(txtTaille.equals("")) && !(txtAge.equals(""))) {
                    double IMG = calculerIMG(Float.parseFloat(txtTaille) / 100, Integer.parseInt(txtPoids), Integer.parseInt(txtAge));
                    if (sexe == 1) {
                        if (IMG < 10) {
                            lblIMG.setTextColor(Color.RED);
                            imgSmiley.setImageResource(R.drawable.maigre);
                        } else if (IMG > 10 && IMG < 25) {
                            lblIMG.setTextColor(Color.GREEN);
                            imgSmiley.setImageResource(R.drawable.normal);
                        } else {
                            lblIMG.setTextColor(Color.RED);
                            imgSmiley.setImageResource(R.drawable.graisse);
                        }
                    } else {
                        if (IMG < 15) {
                            lblIMG.setTextColor(Color.RED);
                            imgSmiley.setImageResource(R.drawable.maigre);
                        } else if (IMG > 15 && IMG < 30) {
                            lblIMG.setTextColor(Color.GREEN);
                            imgSmiley.setImageResource(R.drawable.normal);
                        } else {
                            lblIMG.setTextColor(Color.RED);
                            imgSmiley.setImageResource(R.drawable.graisse);
                        }
                    }
                    lblIMG.setText("IMG :" + String.format("%.01f", IMG));
                } else {
                    Toast.makeText(activite_une.this, "Veuillez saisir tous les champs !", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private float calculerIMG(float taille, int poids, int age) {
        Float IMG;
        IMG = (float) (1.2 * (poids / (taille * taille)) + (0.23 * age) - (10.8 * sexe) - 5.4);
        return IMG;
    }
}
